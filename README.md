###### A Simple Webpage Crawler for Parsing Book Detail from ``books.toscrape.com``

## Requirement
- Scrapy 1.5.1

## How to run
- Open Terminal
- Go inside the project folder having scrapy.cfg
- Input the following command in the terminal

    ``scrapy crawl books``

